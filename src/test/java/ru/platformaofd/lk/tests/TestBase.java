package ru.platformaofd.lk.tests;

import org.junit.After;
import org.junit.Before;
import ru.platformaofd.lk.app.Application;

public class TestBase {
    protected Application app;

    @Before
    public void start() {
        app = new Application();
    }

    @After
    public void quit() {
        app.quit();
    }
}
