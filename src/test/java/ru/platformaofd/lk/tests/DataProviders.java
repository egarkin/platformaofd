package ru.platformaofd.lk.tests;

import com.tngtech.java.junit.dataprovider.DataProvider;
import ru.platformaofd.lk.model.Client;

public class DataProviders {

    @DataProvider
    public static Object[][] validLoginData() {
        return new Object[][]{
                {new Client()
                        .withPhone("79001000001")
                        .withPassword("testtesttest123")
                        .withConfirmationCode("12345")
                }
        };
    }
}
