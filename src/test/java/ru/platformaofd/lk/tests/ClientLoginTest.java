package ru.platformaofd.lk.tests;

import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.platformaofd.lk.model.Client;

@RunWith(DataProviderRunner.class)
public class ClientLoginTest extends TestBase {

    @Test
    @UseDataProvider(value = "validLoginData", location = DataProviders.class)
    public void canLoginClient(Client client) {
        app.loginAs(client);
        String actualProfilePhone = app.getProfilePhone();
        Assert.assertEquals(client.getPhone(), actualProfilePhone);
    }
}
