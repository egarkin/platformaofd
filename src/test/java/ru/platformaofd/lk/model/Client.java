package ru.platformaofd.lk.model;

public class Client {
    private String phone;
    private String password;
    private String confirmationCode;

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public Client withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public Client withPassword(String password) {
        this.password = password;
        return this;
    }

    public Client withConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
        return this;
    }
}
