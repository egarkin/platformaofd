package ru.platformaofd.lk.app;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.platformaofd.lk.model.Client;
import ru.platformaofd.lk.pages.AccountPage;
import ru.platformaofd.lk.pages.ConfirmationCodePage;
import ru.platformaofd.lk.pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Application {
    private WebDriver driver;
    private LoginPage loginPage;
    private ConfirmationCodePage confirmationCodePage;
    private AccountPage accountPage;

    public Application() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        loginPage = new LoginPage(driver);
        confirmationCodePage = new ConfirmationCodePage(driver);
        accountPage = new AccountPage(driver);
    }

    public void loginAs(Client client) {
        loginPage.open();
        if (loginPage.isOnThisPage()) {
            loginPage.enterPhone(client.getPhone());
            loginPage.enterPassword(client.getPassword());
            loginPage.submitLogin();
            if (confirmationCodePage.isOnThisPage()) {
                confirmationCodePage.submitConfirmationCode(client.getConfirmationCode());
            }
        }
    }

    public String getProfilePhone() {
        if (accountPage.isOnThisPage()) {
            return accountPage.getLogin().replaceAll("[\\+\\(\\)\\s\\-]", "");
        }
        return "";
    }

    public void quit() {
        driver.quit();
    }
}
