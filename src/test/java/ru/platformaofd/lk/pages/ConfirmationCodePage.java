package ru.platformaofd.lk.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ConfirmationCodePage extends Page {

    public ConfirmationCodePage(WebDriver driver) {
        super(driver);
    }

    public boolean isOnThisPage() {
        return driver.findElements(By.id("code_confirmation_form_id")).size() > 0;
    }

    public void submitConfirmationCode(String confirmationCode) {
        driver.findElement(By.id("code")).clear();
        driver.findElement(By.id("code")).sendKeys(confirmationCode);
    }
}
