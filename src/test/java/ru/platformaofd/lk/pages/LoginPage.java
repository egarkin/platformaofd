package ru.platformaofd.lk.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Page {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage open() {
        driver.get("https://lk.platformaofd.ru/web/index");
        driver.get("https://t.developer:Eigh4tha@lk.platformaofd.ru/web/api/test/login");
        return this;
    }

    public boolean isOnThisPage() {
        return driver.findElements(By.id("login_form_id")).size() > 0;
    }

    public LoginPage enterPhone(String phone) {
        driver.findElement(By.id("j_username")).clear();
        driver.findElement(By.id("j_username")).sendKeys(phone);
        return this;
    }

    public LoginPage enterPassword(String password) {
        driver.findElement(By.id("j_password")).clear();
        driver.findElement(By.id("j_password")).sendKeys(password);
        return this;
    }

    public void submitLogin() {
        driver.findElement(By.cssSelector("button.btn.btn-primary.btn-block")).click();
    }
}
