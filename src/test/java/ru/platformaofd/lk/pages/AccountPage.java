package ru.platformaofd.lk.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AccountPage extends Page {
    public AccountPage(WebDriver driver) {
        super(driver);
    }

    public boolean isOnThisPage() {
        return driver.findElements(By.id("code_confirmation_form_id")).size() > 0;
    }

    public String getLogin() {
        return driver.findElement(By.cssSelector("span.profile__number.js-field-phone-format-only_initialized"))
                .getText();
    }
}
